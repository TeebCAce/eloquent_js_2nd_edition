// Loop for a triangle

var x = 0,
    c = "";
for ( x; x < 7; x++ ) {
     console.log(c += "#");
}

/*
  #
  ##
  ###
  ####
  #####
  ######
  #######
*/


// FizzBuzz

var x = 1
for ( x; x <= 100; x++ ) {
    x % 5 === 0 && x % 3 === 0 ? console.log("FizzBuzz") :
    x % 3 === 0 ? console.log("Fizz") :
    x % 5 === 0 && x % 3 === 1 ? console.log("Buzz") :
    console.log(x);
}


//Chess game

var s = 8,
    board = "";

for ( var x = 0; x <= s; x++ ) {
  for ( var y = 0; y <= s; y++ ) {
    board += (x + y) % 2 === 0 ? "#" : " ";
   }
   board += "\n";
}

console.log(board);



// CH3  find min between 2 numbers
function findMin (a, b) {
  return a === b || a < b ? a : b;
}


// CH3 Recursion
function isEven(n) {
  if (n == 0)
    return true;
  else if (n == 1)
    return false;
  else if (n < 0)
    return isEven(-n);
  else
    return isEven(n - 2);
}

//CH3 counting beans
function countChar(string, ch) {
  var counted = 0;
  for (var i = 0; i < string.length; i++)
    if (string.charAt(i) == ch)
      counted += 1;
  return counted;
}

function countBs(string) {
  return countChar(string, "B");
}

//CH4 function sum of the range
function range(x, y, step) {

  step = step || 1;
  var arr = [];

  if (step > 0) {
    for (var i = start; i <= end; i += step)
      arr.push( i );
  } else {
    for (var i = start; i >= end; i += step)
      arr.push( i );
  }

  return arr;

}

function sum(array) {
  var total = 0;
  for (var i = 0; i < array.length; i++)
    total += array[i];
  return total;
}


//CH4 Reverse array

function reverseArray(array) {
  var output = [];
  for (var i = array.length - 1; i >= 0; i--)
    output.push(array[i]);
  return output;
}

function reverseArrayInPlace(array) {
  for (var i = 0; i < Math.floor(array.length / 2); i++) {
    var old = array[i];
    array[i] = array[array.length - 1 - i];
    array[array.length - 1 - i] = old;
  }
  return array;
}
